import numpy as np
import wfdb
import os.path
from scipy.signal import resample


def data_reader(mitbih_folder):
    hb_list = [x[:-4] for x in os.listdir(mitbih_folder) if x.endswith(".dat")]
    annotations = []
    sigs = []
    records = []
    for sam in hb_list:
        records.append(sam)
        x = wfdb.rdsamp(mitbih_folder + sam)
        an = wfdb.rdann(mitbih_folder + sam, 'atr')
        an_sam = an.sample
        an_sym = [ord(ele) for ele in an.symbol]
        x_0 = x[0]
        x_M = np.max(x_0)
        x_m = np.min(x_0)
        x_r = (x_M - x_m) / 2
        x_c = x_m + x_r
        x_0 = np.int16(((x_0 - x_c) / x_r) * 2 ** 15)
        x_sh = np.shape(x_0)
        y = np.zeros((x_sh[0] + 1, x_sh[1]))
        y[:-1] = x_0
        y[-1] = np.int16([int(sam), x[1]['fs']])
        z = [np.append(an_sam, int(sam)), np.append(an_sym, int(sam))]
        sigs.append(y)
        annotations.append(z)
        sig = {records[i]: sigs[i] for i in range(len(records))}
        anno = {records[i]: annotations[i] for i in range(len(records))}
    return sig, anno


def beats_splitter(sig, anno):
    ba = ['N', 'L', 'R', 'A', 'a', 'J', 'S', 'V', 'r', 'F', 'e', 'j', 'n', 'E', '/', 'f', 'Q',
          '?']  # beat annotation all
    nba = ['[', '!', ']', 'x', '(', ')', 'p', 't', 'u', '`', '\'', '^', '|', '~', '+', 's', 'T', '*', 'D', '=', '"',
           '@']  # no
    ba_asc = [ord(ele) for ele in ba]  # Convert to ASCII
    nba_asc = [ord(ele) for ele in nba]  # Convert to ASCII
    beats = []
    records = []
    # now need to look in list and break out
    for k in anno.files:  # Loop through each patient
        hb = sig[k]  # Load in each HB signal
        if str(int(hb[-1, 0])) == k:  # Check if patient ID matches the current patient in loop.
            records.append(k)
        else:
            print('Mismatch')
            break
        ba_m = np.nonzero(np.isin(anno[k][1][:-1], ba_asc))[0]  # Filter for only beat annotations and create mask
        be = np.array((anno[k][0][ba_m], anno[k][1][ba_m]))  # Pull out all relevant beat annotations
        df = np.diff(be[0][:-1])  # Find beat length (# of samples per beat)
        d_M = np.max(df)  # Find max beat length
        b_arr = np.zeros((d_M + 2, (len(df) * 2)))  # Pad array to fit every beat for this patient
        for gz in range(len(df)):  # Loop through all beats
            b_arr[:df[gz], (gz * 2):(gz * 2 + 2)] = hb[be[0][gz]:be[0][gz + 1]]  # Put each beat into padded array
            b_arr[-1, (gz * 2):(gz * 2 + 2)] = [anno[k][1][-1],
                                                be[1][gz]]  # Label each beat with patient ID and annotation
        beats.append(np.int16(b_arr))  # Save each patients' array of beats
    beat_rec = {records[i]: beats[i] for i in
                range(len(records))}  # Make a dictionary of the patient IDs and the patient beats.
    return beat_rec


def cropping_outliers(beats_data):
    beats_data_lst = beats_data.files
    beats = [beats_data[x] for x in beats_data_lst]

    crop = int(np.mean([x.shape[0] for x in beats]))
    cropped_beats = np.zeros((crop + 1, 1))

    for hb in beats:
        hb_sh = np.shape(hb)
        if hb_sh[0] < crop + 1:
            z = np.zeros((crop + 1, hb_sh[1]))
            z[:hb_sh[0] - 1] = hb[:-1]
            z[-1] = hb[-1]
        elif hb_sh[0] > crop + 1:
            drop = np.nonzero(hb[crop + 1])[0]
            if np.all(np.diff(drop)[::2] != 1):
                print('ERROR ', drop)
            z = np.zeros((crop + 1, hb_sh[1]))
            z[:crop] = hb[:crop]
            z[-1] = hb[-1]
            z = z[:, np.where(hb[crop + 1] == 0)[0]]
        cropped_beats = np.append(cropped_beats, z, axis=1)

    cropped_beats = cropped_beats[:, 1:].astype(np.int16)
    return cropped_beats


def beatid_str_super(beats_id,map,custom_mapping,num=False):
    cm = [np.array([ord(y) for y in x]) for x in custom_mapping]
    if num:
        beats_class_ch = np.array([[ord(z) for z, y in zip(map, cm) if x in y] for x in beats_id])
    else:
        beats_class_ch = np.array([[z for z, y in zip(map, cm) if x in y] for x in beats_id])
    return beats_class_ch


def dataset_splitter(bcr, use_l1):
    map = ['N', 'S', 'V', 'F', 'X', 'Q']
    custom_mapping = [
        # <--- Normal Class
        ['N', 'L', 'R', 'B'],
        # S class
        ['A', 'a', 'J', 'S', 'e', 'j', 'n'],
        # <--- VEB
        ['V', 'r', 'E'],
        # <--- FUSION
        ['F'],
        # <--- Paced beats are unmapped - don't use record containing paced beats (mitdb - 102,104,107,217)
        ['f', '/'],
        # <--- Unrecognised or Unclassifiable
        ['Q', '?']
    ]
    Supermulticlass = True

    l1_holdout = np.array([200, 203, 207, 217, 223])
    l2_holdout = np.array([102, 116, 200, 203, 210, 223])

    if use_l1:
        test_id = l1_holdout
        fnam = 'BeatsL1PcaSplit_360'
    else:
        test_id = l2_holdout
        fnam = 'BeatsL2PcaSplit_360'

    bcrop = bcr[bcr.files[0]]
    idcrop = bcrop[-1, ::2]
    if Supermulticlass:
        bcrop[-1, 1::2] = beatid_str_super(bcrop[-1, 1::2],map,custom_mapping,num=True).reshape(-1)

    train_id = np.unique(idcrop)
    for i in range(len(test_id)):
        train_id = train_id[np.where(train_id != test_id[i])]
    trbid1 = np.concatenate([np.where(bcrop[-1, ::2] == x) for x in train_id], axis=1).reshape(-1)
    tsbid1 = np.concatenate([np.where(bcrop[-1, ::2] == x) for x in test_id], axis=1).reshape(-1)
    trbid = np.sort(np.concatenate([trbid1 * 2, trbid1 * 2 + 1], axis=0))
    tsbid = np.sort(np.concatenate([tsbid1 * 2, tsbid1 * 2 + 1], axis=0))
    bc = bcrop.copy()
    trb = bc.T[trbid]
    tsb = bc.T[tsbid]
    # at this point you have both beats for the patients you found in the 2 sets just need to decide how to divide up further
    trby = np.append([trb[::2, -1]], [trb[1::2, -1]], axis=0).T
    btrby = np.int16(np.append([trb[::2, -1]], [np.where(trb[1::2, -1] == ord(map[0]), ord(map[0]), ord("@"))], axis=0).T)  # Python logic converts int16 to int64, np.int16 to get back to int16
    trb1 = np.concatenate([trb[::2, :-1], trby], axis=1)
    trb2 = np.concatenate([trb[1::2, :-1], trby], axis=1)
    trbd = np.concatenate([np.subtract(trb[::2, :-1], trb[1::2, :-1]), trby], axis=1)
    btrb1 = np.concatenate([trb[::2, :-1], btrby], axis=1)
    btrb2 = np.concatenate([trb[1::2, :-1], btrby], axis=1)
    btrbd = np.concatenate([np.subtract(trb[::2, :-1], trb[1::2, :-1]), btrby], axis=1)
    tsby = np.append([tsb[::2, -1]], [tsb[1::2, -1]], axis=0).T
    btsby = np.int16(np.append([tsb[::2, -1]], [np.where(tsb[1::2, -1] == ord(map[0]), ord(map[0]), ord("@"))], axis=0).T)  # Python logic converts int16 to int64, np.int16 to get back to int16
    tsb1 = np.concatenate([tsb[::2, :-1], tsby], axis=1)
    tsb2 = np.concatenate([tsb[1::2, :-1], tsby], axis=1)
    tsbd = np.concatenate([np.subtract(tsb[::2, :-1], tsb[1::2, :-1]), tsby], axis=1)
    btsb1 = np.concatenate([tsb[::2, :-1], btsby], axis=1)
    btsb2 = np.concatenate([tsb[1::2, :-1], btsby], axis=1)
    btsbd = np.concatenate([np.subtract(tsb[::2, :-1], tsb[1::2, :-1]), btsby], axis=1)
    # last 2 columns are the patient id and beat anno
    return fnam,trb1,trb2,trbd,trby,tsby,tsb1,tsb2,tsbd,btrb1,btrb2,btrbd,btrby,btsby,btsb1,btsb2,btsbd


def downsample_dataset(f_down):
    sig_dat = ['trc1', 'trc2', 'trcd', 'tsc1', 'tsc2', 'tscd']
    og = 360
    ds_r = [60, 90, 120, 180, 240]
    for g in f_down:
        X = np.load(g)
        print(X.files)
        for f in ds_r:
            t_data = []
            for j in X.files:
                temp = X[j].copy()
                lt = temp[:, :-2].shape[1]
                if j in sig_dat:
                    rsmp = np.int16(np.append([resample(temp[z, :-2], int(lt * f / og)) for z in range(temp.shape[0])], temp[:, -2:], axis=1))
                    t_data.append(rsmp)
                else:
                    t_data.append(temp)
            nfn = g[:-8] + "_" + str(f)
            np.savez_compressed(nfn, trc1=t_data[0], trc2=t_data[1], trcd=t_data[2], trcy=t_data[3], tscy=t_data[4], tsc1=t_data[5], tsc2=t_data[6], tscd=t_data[7])
            print(nfn, t_data[0].shape, t_data[1].shape)
    return


norm = input("Which norm would you like to use for dataset splitting? (l1 or l2)") # Specify which norm to use for splits.
if norm.lower() == "l1":
    use_l1 = True
else:
    use_l1 = False

directories = ["./ds/l1","./ds/l2"]
for i in range(len(directories)):  # Create the directories for l1 and l2 datasets to be saved in.
    if not os.path.exists(directories[i]):
        os.makedirs(directories[i])

mitbih_folder = './mit-bih-arrhythmia-database-1.0.0/'
sigs, annos = data_reader(mitbih_folder)  # Load in the MIT BIH Arrythmia Database, the signals and annotations.
for k in sigs:
    sigs[k] = sigs[k].astype(np.int16)
np.savez_compressed('./ds/sigs_full',**sigs)
np.savez_compressed('./ds/anno_full',**annos)

sig=np.load('./ds/sigs_full.npz')
anno=np.load('./ds/anno_full.npz')
beat_rec = beats_splitter(sig, anno)  # Split each of the ECG signals up by beats.
np.savez_compressed('./ds/beats_full',**beat_rec)  # Save all patient beats

beats_data = np.load('./ds/beats_full.npz')
d = cropping_outliers(beats_data)  # Remove some outlier beats that are more than 3 seconds long.
np.savez_compressed('./ds/beats_cropped',d)

bcr = np.load('./ds/beats_cropped.npz')
fnam,trb1,trb2,trbd,trby,tsby,tsb1,tsb2,tsbd,btrb1,btrb2,btrbd,btrby,btsby,btsb1,btsb2,btsbd = dataset_splitter(bcr,use_l1)  # Split patients into training and testing splits based on the patients specified by the norm.
if use_l1:
    np.savez_compressed('./ds/l1/Multiclass' + fnam, trc1=trb1, trc2=trb2, trcd=trbd, trcy=trby, tscy=tsby, tsc1=tsb1, tsc2=tsb2, tscd=tsbd)
    np.savez_compressed('./ds/l1/Binary' + fnam, trc1=btrb1, trc2=btrb2, trcd=btrbd, trcy=btrby, tscy=btsby, tsc1=btsb1, tsc2=btsb2, tscd=btsbd)
    f_down = ['./ds/l1/BinaryBeatsL1PcaSplit_360.npz', './ds/l1/MulticlassBeatsL1PcaSplit_360.npz']  # Specify dataset names based on the norm
else:
    np.savez_compressed('./ds/l2/Multiclass' + fnam, trc1=trb1, trc2=trb2, trcd=trbd, trcy=trby, tscy=tsby, tsc1=tsb1, tsc2=tsb2, tscd=tsbd)
    np.savez_compressed('./ds/l2/Binary' + fnam, trc1=btrb1, trc2=btrb2, trcd=btrbd, trcy=btrby, tscy=btsby, tsc1=btsb1, tsc2=btsb2, tscd=btsbd)
    f_down = ['./ds/l2/BinaryBeatsL2PcaSplit_360.npz', './ds/l2/MulticlassBeatsL2PcaSplit_360.npz']  # Specify dataset names based on the norm

downsample_dataset(f_down)  # Downsample the full datasets
